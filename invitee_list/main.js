    var people = (function() {
        var people = [];

        //cacheDOm
        var $el = $('.addPerson');
        var $btnAdd = $el.find('.btnAdd');
        var $btnDel = $el.find('.btnDel');
        var $input = $el.find('input');
        var $peoples = $el.find('.peopleContainer');
        var template = $el.find('#people-template').html();

        //bind events
        $btnAdd.on('click', addPerson);
        $peoples.delegate('i.del', 'click', deletePerson);
        $input.on('keypress', enterKeyAdd('onInput'));
        $btnDel.on('click', deleteAll);
        $peoples.delegate('div.itemText', 'click', strikeText);
        $peoples.delegate('span.editText', 'click', editItem);
        $peoples.delegate('span.saveText', 'click', saveItem);
        $peoples.delegate('input.editTextInput', 'keypress', enterKeyAdd('onEdit'));
        $peoples.delegate('span.cancelEdit', 'click', cancelEdit);


        function render(msg, index) {
            if (msg === "delete") {

                $peoples.children().eq(index).slideUp(350, function() {
                    this.remove();
                })
            }
            console.log(people[0]);
            if (msg === "add") {

                $peoples.prepend(Mustache.render(template, people[0]));
                $peoples.children().eq(0).css('display', 'none');
                $peoples.children().eq(0).slideDown(300, function() {
                    $(this).css("display", "block");
                    notifyAdded(this);
                })

            }

            if (msg === "deleteAll") {
                $peoples.slideUp(400, function() {
                    $peoples.html("");
                    $peoples.show();
                });


            }
            //stats.getPeople(people);
        }

        function addPerson(value) {
            var name = (typeof value == "string") ? value : $input.val();
            if (name.replace(/\s/g, '').length) {
                people.unshift(name);
                $input.val('');
                render("add", null);
            } else {
                warnEmpty('.adder');
            }
        }

        function deletePerson(event) {

            if (typeof event === 'number') {
                var i = event;
            } else {
                var $remove = $(event.target).closest('.peopleItem');
                //  console.log($remove.text());
                var i = $peoples.find('.peopleItem').index($remove);
            }
            people.splice(i, 1);
            render("delete", i);

        }

        function enterKeyAdd(msg) {
            if (msg == "onInput") {
                return function(event) {
                    if (event.which == 13) {
                        addPerson();
                    }
                }
            } else if (msg == "onEdit") {
                return function(event) {
                    if (event.which == 13) {
                        console.log('kk');
                        saveItem($(this).parent(this));
                    }
                }
            }
        }

        function deleteAll() {
            people = [];
            render("deleteAll", null);
        }

        function strikeText(event) {
            if (typeof event === 'number') {
                var i = event;
                $peoples.children().eq(i).find('.itemText').css("text-decoration", "line-through");
                strikeNotify($peoples.children().eq(i));
            } else {
                if ($(event.target).css("text-decoration") == "line-through") {
                    $(this).css({
                        "text-decoration": "none"
                    })
                } else {
                    $(event.target).css("text-decoration", "line-through");
                    strikeNotify(event.target.closest('.peopleItem'));
                }
            }

        }

        function editItem() {
            var $peopleItem = $(this).closest('.peopleItem');
            var $editTextInput = $peopleItem.find('input.editTextInput');
            $editTextInput.val($peopleItem.find('.itemText').text());
            console.log($editTextInput.val().length);
            $peopleItem.addClass('edit');
            $editTextInput.focus();
            $editTextInput[0].setSelectionRange(0, $editTextInput.val().length);
        }

        function saveItem($this) {
            if ($this.type) {
                var $peopleItem = $(this).closest('.peopleItem');
            } else {
                var $peopleItem = $($this).closest('.peopleItem');
            }

            //  console.log($remove.text());
            var i = $peoples.find('.peopleItem').index($peopleItem);
            console.log($peopleItem);
            var $editTextInput = $peopleItem.find('input.editTextInput');
            //console.log($editTextInput);

            if (!$editTextInput.val().replace(/\s/g, '').length) {
                warnEmpty($peopleItem);
            } else {
              $peopleItem.find('.itemText').text($editTextInput.val());
                $peopleItem.removeClass('edit');
                people[i]=$editTextInput.val();
            }
        }

        function cancelEdit() {
            var $peopleItem = $(this).closest('.peopleItem');
            $peopleItem.removeClass('edit');

        }
        return {
            addPerson: addPerson,
            deletePerson: deletePerson,
            strikeText: strikeText,
            peopleList: people
        };
    })();







    //
    // var people={
    //   people: ['abc','abc2']
    // }
    //
    // function init(){
    //   var temp = $('#people-template').html();
    //   $('.peopleContainer').html(Mustache.render(temp,people));
    // }
    //
    // init();
