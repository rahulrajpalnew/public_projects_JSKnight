var stats = (function() {
    var $stats = $('.statsContainer');
    var template = $('#stats-template').html();
    var peoples = 0;
    render();

    function render() {
        var count = {
            count: peoples
        };
        //console.log(count);
        $stats.html(Mustache.render(template, count));
    }

    function getPeople(people) {
        peoples = people.length;
        render();
    }

    return {
        getPeople: getPeople
    };

})();
