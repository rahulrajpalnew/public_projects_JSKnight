function warnEmpty(element){
  $(element).transition({background:"lightpink"},200,"snap");
  $(element).transition({background:"transparent"},1200,"snap");
}

function notifyAdded(element){
  $(element).transition({background:"#4ffc05"},10,"ease");
  $(element).transition({background:"transparent"},1300,"ease");
}

function strikeNotify(element){
  $(element).transition({background:"#f44b42"},10,"ease");
  $(element).transition({background:"transparent"},1300,"ease");
}
