## Invitation List UI design with API based back-end.
Built using jQuery and bootstrap which makes this a responsive product.
> What do I mean by API based ?

It simply creates some useful functions for you which makes this super fexible to use and integrate with your app. Just hit the endpoint to add an invitee, delete an invitee and a lot more.

Here's the Behance link to have a feel of the UI

``` https://www.behance.net/gallery/49017173/Invitation-ToDo-List-Add-delete-strikeOff ```


Some sample API calls:

```
people.addPerson("John")

people.deletePerson(*index*)

people.strikeText(*index*)

people.peopleList   //to access the array containing list of all the persons
```


> When to use this ?

If you need a template based dynamic invitation list OR ToDo list in your static website, consider using this.
